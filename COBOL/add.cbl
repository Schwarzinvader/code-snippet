       IDENTIFICATION DIVISION.
       PROGRAM-ID. CALC-ADD.
       AUTHOR. INGRID SCHWARZ.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 NB1 PIC 9(2) VALUE 12.
       01 NB2 PIC 9(2) VALUE 45.
       01 ANNONCE PIC X(20) VALUE "Le résultat est : ".  

       PROCEDURE DIVISION.
           ADD NB1 TO NB2
           DISPLAY ANNONCE, NB2.
           STOP RUN. 
