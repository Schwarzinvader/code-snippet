       IDENTIFICATION DIVISION.
       PROGRAM-ID. STRING.
       AUTHOR. INGRID SCHWARZ.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
      * Phrase qui va être modifié
       01 PHRASE PIC X(40) VALUE "Je suis une phrase".
      * La 1ere chaîne qui sera intégre dans la phrase
       01 P1 PIC X(30) VALUE "Je suis une deuxième phrase".
      * Deuxième chaîne
       01 P2 PIC X(1) VALUE "!".
      * Pointeur
       01 POINTEUR PIC 9(2).

       PROCEDURE DIVISION.
           MOVE 1 TO POINTEUR
      * On assemble les 2 chaînes
           STRING P1 SPACE P2
           DELIMITED BY SIZE
      * dans la variable phrase
           INTO PHRASE
           WITH POINTER POINTEUR
           END-STRING
      * On affiche la phrase
           DISPLAY PHRASE.
           STOP RUN.