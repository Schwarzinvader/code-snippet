       IDENTIFICATION DIVISION.
       PROGRAM-ID. UNSTRING.
       AUTHOR. INGRID SCHWARZ.

       DATA DIVISION.    
       WORKING-STORAGE SECTION.
      *Chaine qui va etre decomposée     
       01 CHAINEP PIC X(20) VALUE "JE SUIS UN TEXTE".
      *VARIABLE QUI CONTIENDRONT LES MOTS SÉPARÉS     
       01 MOT1 PIC X(5).
       01 MOT2 PIC X(5).
       01 MOT3 PIC X(5).   
       01 MOT4 PIC X(5).

       PROCEDURE DIVISION.
      *Décomposer la chaîne 
           UNSTRING CHAINEP
      *On délimite les mots par des espaces 
           DELIMITED BY " "
      *Les variables sont mises ici
           INTO MOT1 MOT2 MOT3 MOT4
      *Affichage du résultat
           DISPLAY "PREMIER MOT :" MOT1
           DISPLAY "DEUXIÈME MOT : " MOT2.
           DISPLAY "TROISIÈME MOT :  " MOT3.
           DISPLAY "QUATRIÈMR MOT : " MOT4.
           STOP RUN.