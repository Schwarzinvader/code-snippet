import React from "react";
import "./css/Burger.css"
import {NavLink} from "react-router-dom";

function Burger(){      
return(
<div className="burger">

    <div id="menuToggle">

        <input type="checkbox" />

        <span></span>
        <span></span>
        <span></span>

        <ul id="menu">
            <li><NavLink className="navbar" to="/">Accueil</NavLink></li>
            <li><NavLink className="navbar" to="/page1">page1</NavLink></li>
            <li><NavLink className="navbar" to="/page2">page2</NavLink></li>

        </ul>
      </div>
    </div>
);
};

export default Burger;
