componentDidMount(){
    const apiPath= "http://localhost:8000/api/prices/?id=1"
    axios.get(apiPath)
    .then((response) => response.data)
    .then((data)=>{
        this.setState({
            id: data.id,
            name: data.name,
            price: data.price,
            description: data.description
        })

    })
};

render(){
    return(
        <article className="bloc-tarif">
            <p>{this.state.name}</p>
            <p>{this.state.description}</p>
            <p>{this.state.price}</p>
        </article>
    );
}
